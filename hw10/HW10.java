// This code allows two users to play a tic tac toe game using a multidimensional array
import java.util.Scanner;

public class HW10 {
	public static String getInt(int[] usedNum, int player, int count) {
		Scanner scan = new Scanner(System.in); // create scanner function
		boolean isInt; // boolean created to see if an int is entered or not
		int input; // create variable int
		String returnVal = "initialized"; //String created for being the value going to be returned
		System.out.println("Player " + player+ " input where you want your marker to be: "); //Requests user input out an int
		while (true) {
			int breakOut = 0; //Breakout needs to be 0 in order to break the loop
			isInt = scan.hasNextInt(); //returns true if an int is inputted
			if (isInt != true) {
				System.out.println("Not an int, input another one: "); // print statement requesting new int to be inputted
				scan.next();// "clears" conveyerbelt 
				continue; // continue brings us back to the condition of the while loop
			}
			input = scan.nextInt();//if the user did input an int then the value is passed to variable input
			while ( 1 > input || input >9) {
				System.out.println("Not between 1-9, input another int: "); //print statement requesting another int
				isInt = scan.hasNextInt(); // makes sure the value inputted is in fact an int
				while (isInt != true) {
					System.out.println("Not an int, input another one: ");
					scan.next(); // "clears" conveyerbelt
		      isInt = scan.hasNextInt(); // allows isInt to be changed while in the while loop
				}
				input = scan.nextInt(); // once the value is within the desired range, input is given the value of the inputted int
			}
      //For this for loop, an array is created to store the values (and is passed into this method from the main method so it
      // retains its memory) already used so the same value can't be used already
			for (int i = 0; i < usedNum.length; i++) {
				if (input == usedNum[i]) { // if a value has been used already
					breakOut = 1; // changes breakout to 1 so we stay in this while loop to get a new int
					System.out.println("Entered input is alredy used, input another int: ");
				}
			}
			if (breakOut == 0) {
				usedNum[count] = input; //Makes the current inputted value equal to the current iteration of the array so it can't be used again
				break; // break out of while loop
			}
		}
    // the switch statement basically converts the int that was retrieved from the while loop above into its respective string value
		switch (input) {
		case 1: returnVal = "1";
		break;
		case 2: returnVal = "2";
		break;
		case 3: returnVal = "3";
		break;
		case 4: returnVal = "4";
		break;
		case 5: returnVal = "5";
		break;
		case 6: returnVal = "6";
		break;
		case 7: returnVal = "7";
		break;
		case 8: returnVal = "8";
		break;
		case 9: returnVal = "9";
		break;
		}
		return returnVal;	// string value is returned to the main method
	}
	public static void print(String[][] array) { // method used to print the array
		for (int i = 0; i < array.length; i++) { // for 2 dimensional arrays, two for loops are needed to print values
			for (int k = 0; k<array[i].length;k++) {
				System.out.print(array[i][k]+"   "); // prints all values of the current row with space in between
			}
			System.out.println(); // newline so next row is on a new line
		}
	}
	public static boolean win(String[][] x) { //This method checks if a player has won or not
		boolean returnVal = false; // if no if loops are entered below, the user hasn't won
    // All the if loops below account for all the different combinations possible to win a game of tic tac toe. If an if loops
    // is entered then the condition is met, and returnVal is returned true to the main method to indicate a win
		if (x[0][0].equals(x[0][1]) && x[0][0].equals(x[0][2])) {
			returnVal = true;
		}
		if (x[1][0].equals(x[1][1]) && x[1][0].equals(x[1][2])) {
			returnVal = true;
		}
		if (x[2][0].equals(x[2][1]) && x[2][0].equals(x[2][2])) {
			returnVal = true;
		}
		if (x[0][0].equals(x[1][0]) && x[0][0].equals(x[2][0])) {
			returnVal = true;
		}
		if (x[0][1].equals(x[1][1]) && x[0][1].equals(x[2][1])) {
			returnVal = true;
		}
		if (x[0][2].equals(x[1][2]) && x[0][2].equals(x[2][2])) {
			returnVal = true;
		}
		if (x[0][0].equals(x[1][1]) && x[0][0].equals(x[2][2])) {
			returnVal = true;
		}
		if (x[2][0].equals(x[1][1]) && x[2][0].equals(x[0][2])) {
			returnVal = true;
		}
		return returnVal; // return boolean value to the main method
	}

	public static void main(String[] args) {
		int [] usedNum = new int[9]; // This array has to be created in the main method to retain its memory rather than be created in getInt method
		String[][] array = {{"1", "2", "3"},{"4","5","6"},{"7","8","9"}}; //Create 2D array with all required values
		String firstPlayer, secondPlayer; //create variables to keep track of whose turn it is
		boolean checkWin; // boolean value created for checking if a user has won
		int turn = 0; // create value for the current turn
		print(array); // prints the array using print method
		int count = -1; // this value will be passed to the getInt method so new values can be put into the int array there
	
		while (turn<6) { 
			turn++;
			count++;
			firstPlayer = getInt(usedNum, 1, count); // first player assigned to the returned string in getInt
      // for loop finds where the player wants to place his X
			for (int i = 0; i < array.length; i++) {
				for (int k = 0; k<array[i].length;k++) {
					if (array[i][k].equals(firstPlayer)) { 
						array[i][k] = "X"; //Changes the number in the tic tac toe board into an X to where it corresponds
					}
				}
			}
			print(array); // print array
			checkWin = win(array); // checks for win
			if (checkWin) {
				System.out.println("Player 1 wins"); // if checkwin is returned true (win), this loop is entered and ends the game 
				break;
			}
			if (turn<5) { // player 1 has 1 more turn than player two, so this value is set here accordingly
				count++;
				secondPlayer = getInt(usedNum, 2,count); // Second player string is assigned to the returned string in this method
				for (int i = 0; i < array.length; i++) {
					for (int k = 0; k<array[i].length;k++) {
						if (array[i][k].equals(secondPlayer)) {
							array[i][k] = "O"; // makes the location where the second user chooses equal to O
						}
					}
				}
				print(array); // prints array
				checkWin = win(array); // checks for a win
				if (checkWin) {
					System.out.println("Player 2 wins"); // if player 2 wins then this message is displayed and the while loop is broken
					break;
				}
			}
			else {
				System.out.println("Draw"); // if the number of turns exceeds 5 for player 2, the game is tied
				break;
			}
		}

	}

}