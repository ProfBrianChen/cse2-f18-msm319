// This lab is about writing code that randomly generates a card for a magician trying to practice his magic trick

import java.util.Random;
public class CardGenerator {
    			// main method required for every Java program
   			public static void main(String[] args) {
          Random randGen = new Random();
          int myNumber; //Create variable for the card number 
          int myCardValue; //create variable for the value of the card
          int myCard; // Create variable for the specific card
          double mySuit; //Create variable for the type of suit
          String stringSuit; // create string for the type of suit
          String specialCard; //create variable for strings for special cards, Ace, jack, ect
          myNumber = (int)(Math.random()*52)+1;
          mySuit = myNumber / 13; 
          myCard = myNumber % 13;
          
          stringSuit = "string";
          
          if (mySuit <= 1) {
            stringSuit = "diamonds" ;
          }
          else if (mySuit>1 && mySuit <= 2) {
            stringSuit = "clubs" ; 
          }
          else if (mySuit>2 && mySuit <= 3) {
            stringSuit = "hearts";
          }
          else if (mySuit>3 && mySuit <= 4) {
            stringSuit = "Spades";
          }
          
          myCardValue = 1;
          specialCard = "string";
          
          switch (myCard) {
            case 1: specialCard = "Ace";
              break; 
            case 2: myCardValue = 2;
              break; 
            case 3: myCardValue = 3;
              break;
            case 4: myCardValue = 4;
              break; 
            case 5: myCardValue = 5;
              break; 
            case 6: myCardValue = 6;
              break; 
            case 7: myCardValue = 7;
              break; 
            case 8: myCardValue = 8;
              break; 
            case 9: myCardValue = 9;
              break; 
            case 10: myCardValue = 10;
              break; 
            case 11: specialCard = "Jack";
              break; 
            case 12: specialCard = "Queen";
              break; 
            case 0: specialCard = "King";
              break; 
          }
          if (myCardValue >= 2 && myCardValue <= 10){
            System.out.println("Your card is the " + myCardValue + " of " + stringSuit); 
          }
          else {
            System.out.println("Your card is the " + specialCard + " of " + stringSuit);
          }
          
        }
}
          