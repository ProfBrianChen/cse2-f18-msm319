public class Arithmetic{
  
  public static void main (String args[]){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    double totalCostOfPants;   //total cost of pants
    double totalCostOfShirts;   //total cost of shirts
    double totalCostOfBelts;    // total cost of belts
    
    totalCostOfPants = (numPants) * pantsPrice; //Calculates the total cost of pants  (before tax)
    totalCostOfShirts = (numShirts) * shirtPrice; // Calculate the total cost of shirts (before tax)
    totalCostOfBelts = numBelts * beltCost ; //Calculate the total cost of belts (before tax)
    
    double pantSalesTax; //Create a variable to hold the value for the total sales tax on pants
    double shirtSalesTax; //Create a variable to hold the value for the total sales tax on shirts
    double beltSalesTax; //Create a variable to hold the value for the total sales tax on belts
    
    pantSalesTax = (totalCostOfPants * paSalesTax); //Calculates the sales tax for the pants
    shirtSalesTax = (totalCostOfShirts * paSalesTax); //Calculates the sales tax for the shirts
    beltSalesTax =  (totalCostOfBelts * paSalesTax); //Calculate the sales tax for the belts
    
    double totalCostOfPurchase ; //Create variable for the total cost of the purchase before taxes
    double totalTaxes; //create variable for the total amount of taxes in the purchase
    double totalPaid; //create variable for the total amount of money paid
    
    totalCostOfPurchase = totalCostOfBelts + totalCostOfShirts + totalCostOfPants ; //Calculating the total cost of purchase before taxes
    totalTaxes = pantSalesTax + shirtSalesTax + beltSalesTax ; //calculates the total taxes on the purchase
    totalPaid = totalCostOfPurchase + totalTaxes ; //calculates the total money paid
    
    System.out.println("total cost of pants = " + totalCostOfPants) ; // prints out the total cost of pants
    System.out.println("total cost of shirts = " + totalCostOfShirts) ; // prints the total cost of shirts
    System.out.println("total cost of belts = " + totalCostOfBelts) ; //prints the total charge on belts
    System.out.printf ("sales tax charged on pants = %.2f\n" , pantSalesTax); //prints out the sales tax on the pants
    System.out.printf ("sales tax charged on shirts = %.2f" , shirtSalesTax); //prints out the sales tax for the shirts
    System.out.println(" "); //forms a new line between statements
    System.out.printf ("sales tax charged on belts = %.2f" , beltSalesTax); //prints out the sales tax for the belts
    System.out.println(" "); //forms a new line between statements
    System.out.printf ("total cost (before tax) = %.2f" , totalCostOfPurchase); //prints out the value for the total cost of purchase
    System.out.println(" "); //forms a new line between statements
    System.out.printf ("total taxes = %.2f" , totalTaxes); //prints out the value of the total taxes
    System.out.println(" "); //forms a new line between statements
    System.out.printf ("total paid = %.2f" , totalPaid); //prints the total amount paid for the transaction
    System.out.println(" "); //forms a new line between statements
    
                       }
                       }
                       