import java.util.Scanner; 
import java.util.Random;
public class CrapsSwitch {
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner( System.in ); //Declares and instance of the scanner function
    System.out.println("To randomly cast two die, press 1. To input the values of the two die, press 2"); //prompt user to input 1 or 2
    int askUser = myScanner.nextInt(); //Allows user to input either a 2 or a 1
    int firstDie,secondDie; //Create integer variables for the first and second die
    firstDie = secondDie = 0; // Inputs values for the dies so the compiler doesn't get an error
    
    // If statement where if the user input 1 above, the statement will generate two random variables and move on.
    //If the user input 2, then it will ask the user to input the values of the die they want. 
    // The if statement within this will give an error message if the values are outside the values of dice.
    // The else statement is for if the user doesn't put a value for 1 or 2 above, it will give an error message
    switch (askUser) {
      case 1: firstDie = (int)(Math.random()*6) + 1;  
              secondDie = (int)(Math.random()*6) + 1;
        break; 
      case 2: System.out.println("What is the value of the first die? (1-6)"); 
              firstDie = myScanner.nextInt(); 
              System.out.println("What is the value of the second die? (1-6)");
              secondDie = myScanner.nextInt();
        break; 
      default: System.out.println("Invalid Entry"); 
    }
    
    // Although the homework asked to only use switch statements, I couldn't find a way to incorperate two variables. 
    //I even tried a switch statment inside other switch statements but the output read every value even though there were breaks.
    // I've tried adding in & and | values in the switch statements for mutiple variables, but there were error messages that said there were duplicate case labels and there ws no way around it.
    // The switch statements here are inside of if statements, which are easier to use for booleans which are easier in this case. 
    // Again, I tried my best to only use switch statements and I've been trying for the past 3 hours with no luck and this was the best I could write using switch statements. 
  if (firstDie == 1) {
      switch (secondDie) {
        case (1): System.out.println("Snake Eyes");
        break; 
        case (2): System.out.println("Ace Deuce");
        break;
        case (3): System.out.println("Easy Four");
        break;
        case (4): System.out.println("Fever Five");
        break;
        case (5): System.out.println("Easy Six");
        break;
        case (6): System.out.println("Seven Out");
        break;
      }
    }
    else if (firstDie == 2){
      switch (secondDie) {
        case (1): System.out.println("Ace Deuce");
        break;
        case (2): System.out.println("Hard Four");
        break;
        case (3): System.out.println("Fever Five");
        break;
        case (4): System.out.println("Easy Six");
        break;
        case (5): System.out.println("Seven Out");
        break;
        case (6): System.out.println("Easy Eight");
        break;
      }
    }
    else if (firstDie == 3){
      switch (secondDie) {
        case (1): System.out.println("Easy Four");
        break;
        case (2): System.out.println("Fever Five");
        break;
        case (3): System.out.println("Hard Six");
        break;
        case (4): System.out.println("Seven Out");
        break;
        case (5): System.out.println("Easy Eight");
        break;
        case (6): System.out.println("Nine");
        break;
      }
    }
    else if (firstDie == 4){
      switch (secondDie) {
        case (1): System.out.println("Fever Five");
        break;
        case (2): System.out.println("Hard Four");
        break;
        case (3): System.out.println("Seven Out");
        break;
        case (4): System.out.println("Hard Eight");
        break;
        case (5): System.out.println("Nine");
        break;
        case (6): System.out.println("Easy Ten");
        break;
      }
    }
    else if (firstDie == 5){
      switch (secondDie) {
        case (1): System.out.println("Easy Six");
        break;
        case (2): System.out.println("Seven Out");
        break;
        case (3): System.out.println("Easy Eight");
        break;
        case (4): System.out.println("Nine");
        break;
        case (5): System.out.println("Hard Ten");
        break;
        case (6): System.out.println("Yo-leven");
        break;
      }
    }
    else if (firstDie == 6){
      switch (secondDie) {
        case (1): System.out.println("Seven Out");
        break;
        case (2): System.out.println("Easy Eight");
        break;
        case (3): System.out.println("Nine");
        break;
        case (4): System.out.println("Easy Ten");
        break;
        case (5): System.out.println("Yo-leven");
        break;
        case (6): System.out.println("Boxcars");
        break;
      }
    }
  }
}

  