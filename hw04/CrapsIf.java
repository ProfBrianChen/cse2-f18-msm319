import java.util.Random;
import java.util.Scanner;
public class CrapsIf {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //Declares and instance of the scanner function
    System.out.println("To randomly cast two die, press 1. To input the values of the two die, press 2"); //prompt user to input 1 or 2
    int askUser = myScanner.nextInt(); //Allows user to input either a 2 or a 1
    int firstDie,secondDie; //Create integer variables for the first and second die
    firstDie = secondDie = 0; // Inputs values for the dies so the compiler doesn't get an error
    
    // If statement where if the user input 1 above, the statement will generate two random variables and move on.
    //If the user input 2, then it will ask the user to input the values of the die they want. 
    // The if statement within this will give an error message if the values are outside the values of dice.
    // The else statement is for if the user doesn't put a value for 1 or 2 above, it will give an error message
    if (askUser == 1) {
      firstDie = (int)(Math.random()*6) + 1;  
      secondDie = (int)(Math.random()*6) + 1;
    }
    else if (askUser == 2) {
      System.out.println("What is the value of the first die? (1-6)"); 
      firstDie = myScanner.nextInt(); 
      System.out.println("What is the value of the second die? (1-6)");
      secondDie = myScanner.nextInt();
      if (firstDie < 1 | secondDie < 1 | firstDie > 6 | secondDie > 6) {
        System.out.println("Invalid inputs"); 
      } 
    }
    else {
      System.out.println("Invalid input"); 
    }
    
    //If statement followed by else if statements for every possible combination of dice rolls. 
    // To save space, each roll has an equivalent (except its two of the same number) so the equivalent will hold the same name 
    if (firstDie == 1 & secondDie == 1) {
      System.out.println("Snake Eyes"); 
    }
    else if (firstDie == 2 & secondDie == 2) {
      System.out.println("Hard Four");
    }
    else if (firstDie == 3 & secondDie == 3) {
      System.out.println("Hard Six");
    }
    else if (firstDie == 4 & secondDie == 4) {
      System.out.println("Hard Eight");
    }
    else if (firstDie == 5 & secondDie == 5) {
      System.out.println("Hard Ten");
    }
    else if (firstDie == 6 & secondDie == 6) {
      System.out.println("Boxcars");
    }
    else if (firstDie == 1 & secondDie == 2 | firstDie == 2 & secondDie == 1) {
      System.out.println("Ace Deuce");
    }
    else if (firstDie == 1 & secondDie == 3 | firstDie == 3 & secondDie == 1) {
      System.out.println("Easy Four");
    }
    else if (firstDie == 1 & secondDie == 4 | firstDie == 4 & secondDie == 1) {
      System.out.println("Fever Five");
    }
    else if (firstDie == 1 & secondDie == 5 | firstDie == 5 & secondDie == 1) {
      System.out.println("Easy Six");
    }
    else if (firstDie == 1 & secondDie == 6 | firstDie == 6 & secondDie == 1) {
      System.out.println("Seven Out");
    }
    else if (firstDie == 2 & secondDie == 3 | firstDie == 3 & secondDie == 2) {
      System.out.println("Fever Five");
    }
    else if (firstDie == 2 & secondDie == 4 | firstDie == 4 & secondDie == 2) {
      System.out.println("Easy Six");
    }
    else if (firstDie == 2 & secondDie == 5 | firstDie == 5 & secondDie == 2) {
      System.out.println("Seven Out");
    }
    else if (firstDie == 2 & secondDie == 6 | firstDie == 6 & secondDie == 2) {
      System.out.println("Easy Eight");
    }
    else if (firstDie == 3 & secondDie == 4 | firstDie == 4 & secondDie == 3) {
      System.out.println("Seven Out");
    }
    else if (firstDie == 3 & secondDie == 5 | firstDie == 5 & secondDie == 3) {
      System.out.println("Easy Eight");
    }
    else if (firstDie == 3 & secondDie == 6 | firstDie == 6 & secondDie == 3) {
      System.out.println("Nine");
    }
    else if (firstDie == 4 & secondDie == 5 | firstDie == 5 & secondDie == 4) {
      System.out.println("Nine");
    }
    else if (firstDie == 4 & secondDie == 6 | firstDie == 6 & secondDie == 4) {
      System.out.println("Easy Ten");
    }
    else if (firstDie == 5 & secondDie == 6 | firstDie == 6 & secondDie == 5) {
      System.out.println("Yo-leven");
    }
    
    
    
    
    
    }
 }
    
  
