//This program asks the user to input an integer between 0-100, and then the output will show an encrypted X with an 
// n+1 by n+1 matrix, where n is the inputed value
import java.util.Scanner; // imports scanner function

public class EncryptedX {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in); // sets up scanner
		System.out.println("Input an integer between 0 and 100"); //Output to ask the user to input value between 0 and 100
		boolean isInt = scan.hasNextInt(); //If the user does put in an int this value will read true, if not, will be false
		int userNum = 0; // variable that will be set with the user's input
		boolean keepGoing = true; //True variable to allow to go into the while loop
    // The following while loop allows for the user to only input an integer. The loop will keep asking for an integer
    // until one is inputed
		while (keepGoing) {
			if (isInt) {
				userNum = scan.nextInt(); //Sets userNum equal to the inputted integer
				break; 
			}
			else {
				System.out.println("Not an integer, input another one");
				scan.next(); //clear
				isInt = scan.hasNextInt(); // Re-asks the user to input
			}
		}
    //The following while loop ensures that the user inputs a variable between 0 and 100. If not, the loop will keep asking the user
    // until there is a valid input 
		while (keepGoing) {
			if (userNum <= 100 && userNum >= 0) {
				userNum = userNum + 1; // I added a + 1 here because in the example it stated that the input was 10 but an 11 x 11 matrix showed up
				break; 
			}
			else {
				System.out.println("Not in the desired range, input another integer"); 
				userNum = scan.nextInt(); 
			}
		}
		int star; //Variable used for the * symbols
		int count; // variable I used for count
		int space1 = 1; // I made a variable for the first space that appears in the body of the x, this space will move to the right once for every row
		int space2 = userNum - 2; // variable for the second space, this space moves from the right to left
    
		//The outside for loop ensures that there are userNum + 1 rows
		for (count = 0; count < userNum; count++) {
			if (count == 0) { // This is the only specific value assigned for the first line, since it will be similar for each input
				System.out.print(" "); // The first space
				for (star = 0; star < userNum - 2; star++) { // Prints out 1 less star than the inputed value (recall the userNum+1 earlier)
					System.out.print("*");
				}
				System.out.println(); //newline
			}
			if ((userNum+1) % 2 == 0) { // For even inputs, this line was needed so that there isn't a double space halfway through the X
				if (count == (userNum-1)/2) { // only enters for even inputs when the count is half of what the user inputted
					for (star = 0; star < userNum - 1; star++) {
						if (star == (userNum-1)/2) { // inputs one space between the two identical sides
							System.out.print(" ");
						}
						System.out.print("*"); // inputs *
					}
					System.out.println(); //Newline
					}
				}
			if (count > 0 && count < userNum - 1) { // for every value between count = 1 and userNum
				for (star = 0; star < userNum - 2; star++) { //This for loop has userNum - 1 iterations, and with the two spaces added for each line it will make a Usernum +1 and userNum+1 matrix
					if (star == space1) { //function that places the first space in the appropriate location
						System.out.print(" ");
					}
					if (star == space2 -1) { // function that places the second space in the appropriate location
						System.out.print(" ");
					}
					System.out.print("*");	 
				}
				System.out.println();
				space1++; //Space one increments so the space goes from left to right
				space2--; // space two decrements so that the space goes from right to left
			}
			
			
		}

	}

}