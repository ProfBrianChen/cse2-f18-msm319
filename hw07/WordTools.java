// This method prompts the user to input a phrase, and provides a menu with commands, and asks
// the user to input a letter from the menu that commits a function (ie. count the number of words)
import java.util.Scanner; 

public class WordTools {

	public static String sampleText(Scanner scan) { //Method for sample text. Scan is in the parenthesis because there is a scan function used
		String userInput; // Creates a string variable userInput. This variable is used frequently throughout this code
		System.out.println("Enter a sample text:"); // Asks user to enter text
		userInput = scan.nextLine(); // nextline function allows for capturing a whole paragraph
		System.out.println(); //Space for appearance reasons
		System.out.println("You entered: " + userInput); // Displays what the user wrote in the previous step
		System.out.println();// newline
		return userInput; //Return allows us to use what the user input in this method to be used in the main method, and thus being able to be transferred to other methods
	}
	//FIXME
	public static String printMenu(Scanner scan) { //Once again, scan is used because this method requests a letter from the user
		System.out.println(); // Newline
		System.out.println("Menu"); //The following print statements print out the menu to the user
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replaces all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - quit");
		System.out.println(); // newline
		System.out.println("Choose an option:"); //prompts user to input a letter from the menu
		String userLetter = scan.next(); //Lets the user input a character
		return userLetter; // Returns this character so we can use the string value outputted by this method in the main method
		
	}
	
	public static int getNumOfNonWSCharacters(String userInput) { //Userinput is in the parenthesis because we are transferring what the user input to this method
		// int is stated before the method name because we are returning an int value to the main method, rather than a string
		int count = 0; // Integer for counter
		char currChar; // variable made for the current character in this for loop
		
		//The following for loop uses the function .lenght(); to convert the userInput into singular
		// characters that the for loop will work from right to left with
		for( int i = 0; i < userInput.length(); i = i+1 ){ 
		    currChar = userInput.charAt(i); // current character is equal to the character that the integer i is at as is moves through the input
		    if(currChar != ' ') { // if loop for if the current character is not a space
		        count++; //Only increments the count if the current character isn't a space
		    }
		}
		return count; // returns the integer count to the main method
	}
	
	public static int getNumOfWords(String userInput) { // this method will return an int, hence the int
		
		int numWords = 0; //Variable made for the number of words
		boolean ifWord = false; //boolean value of if the character is a word
	    int end = userInput.length() - 1; //The cutoff is needed or else the for loop will not count the last word

	    for (int i = 0; i < userInput.length(); i = i + 1) {
	    	if (userInput.charAt(i) == '\'') { // I noticed words with apostrophes would count twice
	    		numWords--;  // Everytime an apostrophe shows up subtract a value
	    	}
	        if (Character.isLetter(userInput.charAt(i)) && i != end) { //If the current character is on a letter, then isWord will be true
	            ifWord = true;
	           
	        } 
	        
	        else if (!Character.isLetter(userInput.charAt(i)) && ifWord) { // If the current character comes across a non letter(ie a space) after crossing a word, it will increment the word count
	            numWords++; // increments wordcount
	            ifWord = false; // Returns if word false until another character is reached (to avoid counting a double space as a start of a word)
	        
	        } 
	        else if (Character.isLetter(userInput.charAt(i)) && i == end) { //For when the program reaches the last word
	            numWords++; //Because there is no space after the last word, the counter will not count the last word, which is why this is here
	        }
	    }
	    return numWords; //Returns the integer numWords to main method
	}

	public static int findText(String userInput) { //Method for finding text
		Scanner scan = new Scanner(System.in); // Creates scanner function
		System.out.println("Enter a word or phrase to be found:"); // Asks user for a word or phrase to be found
		String findWord = scan.nextLine(); //Allows the user to input a word
		int foundInst = 0; //Variable for instances a word is found
		foundInst = userInput.split(findWord).length; //Function for finding the user inputed word within the original phrase (userInput)
		findWord = "''" + findWord + "''"; //The only way i knew how to put parentheses around the word of interest
		System.out.print(findWord); // Because this method returns an integer, it can't return this string back to the main method, so it is printed here
		
		return foundInst; //returns the number of instances back to main method
	}
	
	public static void replaceExclamation(String userInput) { //Void is before the method name because no values are being returned to main method
		char change; //Notifies when a character needs to be changed (ie when there's an exclamation)
		StringBuilder changeString = new StringBuilder(userInput); //A new string builder function to change an existing string
		
		//Pretty much a direct copy from the number of non-whitespace characters method
		for(int i = 0; i < userInput.length(); i = i+1 ){
			change = userInput.charAt(i);
		    if (change == '!') { // is true when the current character is an exclamation
		        changeString.setCharAt(i,'.'); //Changes the current string to swap out the exclamation for a period
		    }
		}
		System.out.println(changeString); // Prints out the new string
	}
	public static void shortenSpace(String userInput) { //Method for shortening spaces
		System.out.println(userInput.replaceAll("\\s+", " ")); // Function that replaces all spaces over one space to just one space. 
		
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in); // Sets up scanner function in main method
		String userInput = sampleText(scan); //Gives the string userInput the returned string in the sample text method
		int numChars = 0; // initializes variable
		int numWords = 0;
		int foundInst =0;
		String userLetter = printMenu(scan); // Gives the string userLetter the string returned from the printMenu method
		boolean keepGoing = true; //Aribrary true value
		if (userLetter.equals("q")) {  //Immediately quits, can happen before other options
		}
		else { // for all values other than q
			while (keepGoing) {
				if (userLetter.equals("c")) {
					numChars = getNumOfNonWSCharacters(userInput); // calls getNumOfNonWSCharacters() method and sending over the string userInput. The returned value is an int that will be equal to numChars
					System.out.println("Number of non-whitespace characters: " + numChars); // print
					break; 
				}
				else if (userLetter.equals("w")) {
					numWords = getNumOfWords(userInput); // Gives numWords the returned value from the number of words method
					System.out.println("Number of words: " + numWords); //print
					break; 
				}
				else if (userLetter.equals("f")) {
					foundInst = findText(userInput); //Give foundInst the returned int value from findText method
					System.out.println(" instances: " + foundInst); // prints
					break; 
				}
				else if (userLetter.equals("r")) {
					replaceExclamation(userInput); // Calls the following method and sends the userInput string over
					break; 
				}
				else if (userLetter.equals("s")) {
					shortenSpace(userInput); // calls for the shorten space method
					break; 
				}
				else if (userLetter.equals("q")) {
					break; //breaks loop if quit
				}
				else {
					System.out.println("Invalid entry");
					scan.nextLine();
					userLetter = printMenu(scan);
				}
			}
		}
	}

}