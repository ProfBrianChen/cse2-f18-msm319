public class WelcomeClass{
  
  public static void main (String args[]){
    ///prints the initial 3 lines of the homework assignment
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    ///Prints the next 5 lines, with the characters of my lehigh network ID. Important to note that the firs line of code has 
    /// 2 spaces prior to the carrot symbols and the output has one space before the /. 
    // In order to print a backslash you have to put two \\ to print one - Found on google
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\" );
    System.out.println("<-M--S--M--3--1--9->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  /v  v  v  v  v  v");
                       
                       }
                       }
                       