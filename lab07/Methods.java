// The following lab uses methods and random numbers to generate random sentences with 
// different verbs, adjectives, and nouns
import java.util.Random; //imports random
import java.util.Scanner; // imports scanner

public class Methods {
	
	public static String adjectives() { //method for choosing an adjective
		String myAdj = "initialize"; // initializes myAdj string
		Random randGen = new Random(); // sets up random function
		int randNum = randGen.nextInt(10); // gives randNum a random int from 0-9
		
		//The following switch statement uses the random value generated above to choose from the
		// list of adjectives int he following statement. myAdj will then be assigned the appropriate adjective
		switch (randNum) {
		case 0: myAdj = "quick";
			break; 
		case 1: myAdj = "stinky";
			break;
		case 2: myAdj = "angry";
			break;
		case 3: myAdj = "happy";
			break;
		case 4: myAdj = "confused";
			break;
		case 5: myAdj = "hungry";
			break;
		case 6: myAdj = "tall";
			break;
		case 7: myAdj = "fluffy";
			break;
		case 8: myAdj = "mean";
			break;
		case 9: myAdj = "excited";
			break;
		}
		return myAdj; // Returns myAdj to the main method for use there
	}
	
	public static String subjectNoun() { // Same format as adjective method
		String myNoun = "initialize";
		Random randGen = new Random(); 
		int randNum = randGen.nextInt(10);
		
		//This switch statement does the same thing as the one in the adjective method
		switch (randNum) {
		case 0: myNoun = "fox"; 
			break; 
		case 1: myNoun = "dog";
			break;
		case 2: myNoun = "cat"; 
			break;
		case 3: myNoun = "zebra";
			break;
		case 4: myNoun = "pigeon";
			break;
		case 5: myNoun = "monkey"; 
			break;
		case 6: myNoun = "bee"; 
			break;
		case 7: myNoun = "lion"; 
			break;
		case 8: myNoun = "shoe";
			break;
		case 9: myNoun = "book";
			break;
		}
		return myNoun;
	}
	
	public static String pastTenseVerbs() { // Same format as adjective method
		String myVerb = "initialize";
		Random randGen = new Random(); 
		int randNum = randGen.nextInt(10);
		
		//This switch statement does the same thing as the one in the adjective method
		switch (randNum) {
		case 0: myVerb = "ran to the"; 
			break; 
		case 1: myVerb = "jumped on the";
			break;
		case 2: myVerb = "attacked the";
			break;
		case 3: myVerb = "fell into a";
			break;
		case 4: myVerb = "flicked the";
			break;
		case 5: myVerb = "drank the";
			break;
		case 6: myVerb = "cleaned the"; 
			break;
		case 7: myVerb = "hit the";
			break;
		case 8: myVerb = "slept in the";
			break;
		case 9: myVerb = "ate the";
			break;
		}
		return myVerb; 
	}
	
	public static String objectNoun() { // Same format as adjective method
		String myNoun2 = "initialize";
		Random randGen = new Random(); 
		int randNum = randGen.nextInt(10);
		
		//This switch statement does the same thing as the one in the adjective method
		switch (randNum) {
		case 0: myNoun2 = "house"; 
			break; 
		case 1: myNoun2 = "plane"; 
			break;
		case 2: myNoun2 = "helicopter";
			break;
		case 3: myNoun2 = "towel";
			break;
		case 4: myNoun2 = "hair";
			break;
		case 5: myNoun2 = "hat"; 
			break;
		case 6: myNoun2 = "car"; 
			break;
		case 7: myNoun2 = "bridge"; 
			break;
		case 8: myNoun2 = "cup"; 
			break;
		case 9: myNoun2 = "piglet";
			break;
		}
		return myNoun2;
	}
	
	public static String keepNoun(String subjectNoun) { // method for keeping the same subject noun in action sentence
		Random randGen = new Random(); 
		int randNum = randGen.nextInt(2);//Randomly generates number
		
		//The following if statement uses a random number to either keep the same noun
		// or use "it"
		if (randNum == 0) {
			subjectNoun = "It ";
		}
		else {
			subjectNoun = "The " + subjectNoun + " ";
		}
		return subjectNoun; //Returns string subjectNoun
	}
	
	public static void conclusion(String subjectNoun) {
		System.out.print("That " + subjectNoun); //uses the subjectnoun method above to either use same or "it"
		System.out.print(" " + pastTenseVerbs() + " "); //Randomly generates a past tense verb
		System.out.println(objectNoun() + "!");//uses an object noun as well as a exclamation
	}
	
	public static void finalMethod() {
		
		Random randGen = new Random(); 
		int randNum = randGen.nextInt(3) + 2;
		
		String adj = "Initialized"; //Initialize all variables
		String adj2 = "Initialized";
		String subjectNoun = "Initialized";
		String subjectNoun2 = "Initialized";
		String objNoun = "Initialized";
		String objNoun2 = "Initialized";
		
		System.out.print("The "); // Starts with the to make the sentence flow better
		adj = adjectives(); // sets adj equal to the returned value from the method
		System.out.print(adj); // prints this value
		adj2 = adjectives(); // same thing
		while (adj == adj2) { // while statements to ensure the same adjectives aren't used in the same line
			adj2 = adjectives(); // gives adj2 a new string if needed
		}
		System.out.print(" " + adj2); // prints
		subjectNoun = subjectNoun(); // sets subjectNoun equal to the string returned in the method
		System.out.print(" " + subjectNoun); // prints the string
		System.out.print(" " + pastTenseVerbs()); //prints the result of the method of past tense verb
		System.out.println(" " + objectNoun() + "."); // prints the returned string in the method
		// Line 2
		for (int i = 0; i < randNum; i++) { // random number (up to five) to determine number of action sentences. 
		subjectNoun2 = keepNoun(subjectNoun); // used to keep the same noun throughout 
		System.out.print(subjectNoun2); //prints the result of the call method from the line above
		System.out.print(pastTenseVerbs()); // prints result of the method called
		objNoun = objectNoun(); 
		System.out.print(" " + objNoun);// prints result of the method called
		System.out.print(" at the"); // helps connect the sentence
		System.out.print(" " + adjectives());// prints result of the method called
		objNoun2 = objectNoun();
		while (objNoun == objNoun2) { // this while loop ensures the same object nount isn't used twice in same sentence
			objNoun2 = objectNoun();
		}
		System.out.println(" " + objNoun2 + ".");
		}
		conclusion(subjectNoun); // calls the conclusion with the subjectNoun to keep it 
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int userNum = 1;  //initialize variables
		String adj = "Initialized"; 
		String adj2 = "Initialized";
		String subjectNoun = "Initialized";
		String subjectNoun2 = "Initialized";
		String objNoun = "Initialized";
		String objNoun2 = "Initialized";
		
		boolean keepGoing = true; 
		while (keepGoing) {
			if (userNum == 0) {
				break;
			}
			if (userNum == 1) {
				System.out.print("The "); // copy and paste from final method
				adj = adjectives();
				System.out.print(adj);
				adj2 = adjectives();
				while (adj == adj2) {
					adj2 = adjectives();
				}
				System.out.print(" " + adj2);
				System.out.print(" " + subjectNoun());
				System.out.print(" " + pastTenseVerbs());
				System.out.println(" " +objectNoun() + ".");
				System.out.println("If you want to quit, press 0. If you want another sentence, press 1.  If you want phase 2, press 2");
				// wasn't sure how to include all phases with showing the differences so i did this
				userNum = scan.nextInt(); // allows user to pick an int
			}
			if (userNum == 2) { // enters phase 2
				System.out.print("The "); // copy and paste
				adj = adjectives();
				System.out.print(adj);
				adj2 = adjectives();
				while (adj == adj2) {
					adj2 = adjectives();
				}
				System.out.print(" " + adj2);
				subjectNoun = subjectNoun();
				System.out.print(" " + subjectNoun);
				System.out.print(" " + pastTenseVerbs());
				System.out.println(" " + objectNoun() + ".");
				// Line 2
				subjectNoun2 = keepNoun(subjectNoun);
				System.out.print(subjectNoun2);
				System.out.print(pastTenseVerbs());
				objNoun = objectNoun();
				System.out.print(" " + objNoun);
				System.out.print(" at the");
				System.out.print(" " + adjectives());
				objNoun2 = objectNoun();
				while (objNoun == objNoun2) {
					objNoun2 = objectNoun();
				}
				System.out.println(" " + objNoun2 + ".");
				conclusion(subjectNoun);
				System.out.println();
				System.out.println("The final method:");
				finalMethod(); // here is where the final method comes in and is seperated from the rest
				
				
				System.out.println("If you want to quit, press 0. If you want another one-liner, press 1.  If you want another phase 2, press 2");
				userNum = scan.nextInt(); // lets user choose
			}
		}
	}
}