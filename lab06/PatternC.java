import java.util.Scanner;

public class PatternC {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in); 
	    System.out.println("Input Interger between 1 to 10");
	    boolean userTrue = scan.hasNextInt(); 
	    int userNum = 1; 
	    boolean keepGoing = true; 
	    while (keepGoing){
	      if (userTrue){
	       userNum = scan.nextInt(); 
	        break; 
	      }
	       else { 
	        System.out.println("Not an integer, input another one");
	        scan.next();
	        userTrue = scan.hasNextInt(); 
	      }  
	    }
	    while (keepGoing){
	          if (userNum >=1 && userNum <= 10 ){
	            userNum = userNum; 
	            break;
	          }
	          else {
	            System.out.println("Not between 1 and 10, input another integer");  
	            userNum = scan.nextInt();
	          }
	       }
	    int count = 0;
	    
	   // This loop is different because after count is assigned a new value, a the number of spaces will correspond to all the values from 1-10
	   // The if statements need to be outside of the second for loop or else there will be double spacing
	    for (count = 0; count < userNum; count = count + 1){
	    	if (count == 0) {
	    		  System.out.print("           "); 
	    	  }
	    	  if (count == 1) {
	    		  System.out.print("          ");
	    	  }
	    	  if (count == 2) {
	    		  System.out.print("         ");
	    	  }
	    	  if (count == 3) {
	    		  System.out.print("        ");
	    	  }
	    	  if (count == 4) {
	    		  System.out.print("       ");
	    	  }
	    	  if (count == 5) { 
	    		  System.out.print("      ");
	    	  }
	    	  if (count == 6) {
	    		  System.out.print("     ");
	    	  }
	    	  if (count == 7) {
	    		  System.out.print("    ");
	    	  }
	    	  if (count == 8) {
	    		  System.out.print("   ");
	    	  }
	    	  if (count == 9) {
	    		  System.out.print("  ");
	    	  }
	      for (int numRows = count + 1; numRows > 0; numRows = numRows - 1){ // the conditions for this loop decrease the variable numRows, which is initially set to 1 + the counter
	    	  System.out.print(numRows); // Prints numRows
	      }  
	      System.out.println(" "); //Newline
	    }

	}

}