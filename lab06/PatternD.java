import java.util.Scanner;

public class PatternD {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in); 
	    System.out.println("Input Interger between 1 to 10");
	    boolean userTrue = scan.hasNextInt(); 
	    int userNum = 1; 
	    boolean keepGoing = true; 
	    while (keepGoing){
	      if (userTrue){
	       userNum = scan.nextInt(); 
	        break; 
	      }
	       else { 
	        System.out.println("Not an integer, input another one");
	        scan.next();
	        userTrue = scan.hasNextInt(); 
	      }  
	    }
	    while (keepGoing){
	          if (userNum >=1 && userNum <= 10 ){
	            userNum = userNum; 
	            break;
	          }
	          else {
	            System.out.println("Not between 1 and 10, input another integer");  
	            userNum = scan.nextInt();
	          }
	       }
	    int count = 0;
	    
	   
	    for (count = userNum; count > 0; count = count - 1){
	      for (int numRows = count; numRows > 0; numRows = numRows - 1){ //Decreases value numRows
	        System.out.print(numRows); //Prints value numRows
	        System.out.print(" "); //Places space between values
	      }  
	      System.out.println(" "); //Newline
	    }

	}

}