// Lab 6 wants us to input 4 different patterns. The program will prompt the user to input an integer, and the pattern will 
// react differently with the different inputs. 
import java.util.Scanner; // imports the scanner function
public class PatternA {
  public static void main(String[] args) {
    // This whole section (until for statements) are the same for all 4 patterns. I will just comment on this pattern
    // to avoid redundancy 
    Scanner scan = new Scanner(System.in); // Sets scan as the scanner function
    System.out.println("Input Interger between 1 to 10"); // prompts user to input integer from 1-10
    boolean userTrue = scan.hasNextInt(); // sets a boolean value for if the user did indeed input an integer
    int userNum = 1; // declare variable userNum
    boolean keepGoing = true; // Arbitrary true boolean value to enter while loop
    // The following while loop will only accept integers. If a non integer is placed it will keep asking
    while (keepGoing){ 
      if (userTrue){
       userNum = scan.nextInt(); // sets userNum equal to the value inputed by the user
        break; 
      }
       else { 
        System.out.println("Not an integer, input another one");
        scan.next(); // clear
        userTrue = scan.hasNextInt(); // allows the user to input a value again
      }  
    }
    //The following while loop is only satisfied if the user input a value between 1 and 10. 
    while (keepGoing){
          if (userNum >=1 && userNum <= 10 ){
            userNum = userNum; // sets userNum equal to the value it was prior
            break;
          }
          else {
            System.out.println("Not between 1 and 10, input another integer");  
            userNum = scan.nextInt(); // Allows the user to input another value
          }
       }
    int count = 0; // declare variable count
    
   //The following while loop allows us to achieve pattern A
    for (count = 1; count < userNum + 1; count = count + 1){
      for (int numRows = 0; numRows < count ; numRows= numRows){ //numRows is basically a variable that increments and we can use to output its value
        ++numRows; // increments numRows
        System.out.print(numRows); //Prints value of numRows
        System.out.print(" "); // Prints spaces between values
      }  
      System.out.println(" "); //Newline
    }

}
}