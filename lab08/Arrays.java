public class Arrays {

	public static void main(String[] args) {
		int numElements = 100; // value for the number of elements in the array
		int[] firstArray = new int[numElements]; //Creating the first array- firstArray- with 100 elements that are all integers
		for (int i =0; i < numElements; i++) { // for loop that assigns each value in the array a random integer from 0-99
		firstArray[i] = (int)(Math.random()*100); //generate random integer from 0-99
		}
		
		int[] secondArray = new int[numElements]; // Creating second array- secondArray- with 100 elements that are all integers
		for (int j =0; j <numElements; j++) { //This array is progressively increasing by 1, so the first elements is 0, second element is 1 and so on
			secondArray[j] = j; // Assigning the element to the current iteration of i
		}
		for (int k =0; k<numElements; k++) { //The first for loop will progress the second array, so all values between 0 and 99 can be checked
			int count = 0; // starts the count at 0 for each iteration
			for (int l = 0; l<numElements; l++) { //This for loop progresses the second element 
				if (firstArray[l] == secondArray[k]) { 
					count++; // counts each time the second array element at k equals all the values in the first array by progressing the first array 
          // within the first for loop
				}
			}
			if (count != 0) { // Only want to display numbers that show occurences, so it there were no occurences there would be no output
				if (count == 1) { //This is just here for grammar purposes because it had it in the example (time vs times)
					System.out.println(secondArray[k] + " occurs " + count + " time");
				}
				else {
					System.out.println(secondArray[k] + " occurs " + count + " times"); // output statement seen in example
				}
			}
		}

	}

}