import java.util.Arrays;
public class SelectionSortLab10 {
	public static void main(String[] args) {
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest = selectionSort(myArrayBest);
		int iterWorst = selectionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}

	/** The method for sorting the numbers */
	public static int selectionSort(int[] list) { 
		System.out.println(Arrays.toString(list));
		int iterations = 0;
		int temp = 0;

		for (int i = 0; i < list.length - 1; i++) {
			// Step One: Find the minimum in the list[i..list.length-1]
			int currentMin = list[i]; 
			int currentMinIndex = i;
			iterations++;
			System.out.println(Arrays.toString(list));
			for (int j = i + 1; j < list.length; j++) {
				iterations++; //Increments iterations
				if (currentMin > list[j]) {
					currentMinIndex = j; //Change the current index for use later
				}
			}
			if (currentMinIndex != i) { // if the current min index changed
				 temp = list[i]; // temporary value used to store value in list[i]
				 list[i]= list[currentMinIndex] ; //values are swapped
				 list[currentMinIndex] = temp; // the swapped value gets the initial value that was stored in the temp value
			}
		}
		return iterations;
	}
}
