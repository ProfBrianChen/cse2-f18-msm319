import java.util.Arrays;

public class InsertionSortLab10 {
	public static void main(String[] args) {
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest = insertionSort(myArrayBest);
		int iterWorst = insertionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}
	/** The method for sorting the numbers */
	public static int insertionSort(int[] list) {
		System.out.println(Arrays.toString(list));
		System.out.println(Arrays.toString(list));
		int iterations = 0;
		int temp = 0; 
		//For element list[i] in the array.....
		for (int i = 1; i < list.length; i++) {
			iterations++;
			for(int j = i ; j > 0 ; j--){
				System.out.println(Arrays.toString(list)); // prints array
				if(list[j] < list[j-1]){
					iterations++; // increments iterations
					temp = list[j]; // temporary value to hold the current element in the array
					list[j] = list[j-1]; // values of the two elements are switched
					list[j-1] = temp; // swapped element gets value of the first initial element
				}
				else {
					break;
				}				
			}
			System.out.println(Arrays.toString(list));
		}
		return iterations;
	}
}