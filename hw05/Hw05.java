//This code will prompt the user to input the amount of desired loops (hands), then the program will pick 5 random cards
// and show the probabilities of either getting a 4 of a kind, 3 of a kind, two pair, or single pair
import java.util.Scanner; //Imports the scanner function
import java.util.Random; //Imports the random generator function
import java.util.Arrays; //Allows later for organizing an array and having the numbers shown in an increasing order

public class Hw05 {

	public static void main(String[] args) {
		
		Random randGen = new Random(); //sets up a function randgen for use later
		Scanner scan = new Scanner(System.in); //sets up scan function for use later
		System.out.println("Input the amount of hands you want to generate"); //prompts user to input the amount of hands (loops) desired
		boolean genNum = scan.hasNextInt(); //Determines if the user did in fact input an integer
		int count; //A value that will be used in the for statement that acts as a counter
		boolean keepGoing = true; //artibrary true value
		int numGen = 0; // Hardcoding an arbitrary value to avoid variables not being initialized
		
    //The following while loop ensures a user inputs an integer for the number of loops
    // If the user doesn't input an integer, the program keeps asking until the user inputs an integer
		while (keepGoing) {
			if (genNum) {
				numGen = scan.nextInt(); // sets variable numGen equal to the entered integer
				break; //breaks out of while loop
			}
			else {
				System.out.println("Input the amount of hands you want to generate");
				scan.next(); //clears the "conveyer belt"
				genNum = scan.hasNextInt(); //Repeats ability to input an integer, and will be true if it is and false if it isn't
			}
		}
		
    //Initializing all variables that will be used in the for loop to avoid variables not being initialized
		int firstNum = 0;
		int secondNum = 0;
		int thirdNum = 0; 
		int fourthNum = 0; 
		int fifthNum = 0;
		int fourKind = 0; 
		int threeKind = 0; 
		int twoPair = 0; 
		int onePair = 0; 
		
    //The overall for loop is going to repeat as many times as the user inputed by using the count variable
    // The count variable will be incrimented each time until it is greater than the value given by the user,
    // meaning that the program will break out of the for loop when that occurs
		for (count = 0; count < numGen; count = 1 + count) {
			firstNum = (((int)(Math.random()*52)+1)); //Random number generated from 1-52 and is taken by the variable firstNum
			secondNum = (((int)(Math.random()*52)+1)); // random number generated once again, taken by variable secondNum
      //The following while loop ensures that the two randomly generated variables aren't equal to eachother, and in the case
      // that they are, a new random number will be generated until the first generated number and second generated number
      // are no longer equal to eachother. For each random number generated after this one, the process will be repeated,
      //and the only different will be that the constraint for the if statement will include all prior values
			while(keepGoing) {
				if (secondNum != firstNum) {
					secondNum = secondNum; 
					break; 
				}
				else {
					secondNum = (((int)(Math.random()*52)+1)); // generates new number
				}
			} 
				// Same method repeated for third random number
			thirdNum = (((int)(Math.random()*52)+1));
			while(keepGoing) {
				if (thirdNum != secondNum && thirdNum!= firstNum) {
					thirdNum = thirdNum; 
					break; 
				}
				else {
					thirdNum = (((int)(Math.random()*52)+1)); // generates another random number; 
				}
			}
			// Same method repeated
			fourthNum = (((int)(Math.random()*52)+1));
			while(keepGoing) {
				if (fourthNum != thirdNum && fourthNum != secondNum && fourthNum != firstNum) {
					fourthNum = fourthNum; 
					break; 
				}
				else {
					fourthNum = (((int)(Math.random()*52)+1)); 
				}
			}
			// Same method repeated
			fifthNum = (((int)(Math.random()*52)+1));
			while (keepGoing) {
				if (fifthNum != fourthNum && fifthNum!= thirdNum && fifthNum != secondNum && fifthNum != firstNum) {
					fifthNum = fifthNum; 
					break; 
				}
				else {
					fifthNum = (((int)(Math.random()*52)+1)); 
				}
			}
			
      // Because the code only looks at pairs, the values are divided by modulus 13 to get the real numberical value of the
      // card, since suits don't matter for pairs. 
			firstNum = firstNum % 13; 
      secondNum = secondNum % 13; 
      thirdNum = thirdNum % 13; 
      fourthNum = fourthNum % 13; 
      fifthNum = fifthNum % 13; 
			
      //In order to make the constraints for the following if statements smaller and easier to manage, I thought
      // it would be best to organize the values in an Array and have them sorted by the smallest to the largest values. 
      // Obviously, if the values are equal they will be one after another, making it easier and create less constraints
      // for all the different possible combinations. For example, for a two pair there would be 60 different combinations in
      // a 5 card hand!! so organizing in numerical order makes the process easier. 
			int[] myArray = new int[5];
			myArray[0] = firstNum; //sets the first number in the array equal to the first randomly generated number
			myArray[1] = secondNum; //sets the second number in the array equal to the second randomly generated number
			myArray[2] = thirdNum; //sets the third number in the array equal to the third randomly generated number
			myArray[3] = fourthNum; // sets the fourth number in the array equal to the fourth randomly generated number
			myArray[4] = fifthNum; //sets the fifth number in the array equal to the fifth randomly generated number
			Arrays.sort(myArray); // sort array from lowest values to highest values (same values will be next to eachother)
			int val1 = myArray[0]; // Sets lowest number to val1
			int val2 = myArray[1]; // sets second lowest number to val2
			int val3 = myArray[2]; // sets middle number to val3
			int val4 = myArray[3]; //sets second highest number to val4
			int val5 = myArray[4]; // sets largest number to val5
			
			// The following if statements contain the constraints for all the possible outcomes for the 4 different hands. 
      // If the outcome of a hand is met, then a counter in each statement records how many of each hand occured
			if ((val1 == val2 && val2 == val3 && val3 == val4) || (val2 == val3 && val3 == val4) && val4 ==val5) {
				fourKind = fourKind + 1; 
			}
			else if ((val1 == val2 && val2 == val3) || (val2 == val3 && val3 == val4) || (val3 == val4 && val4 == val5)) {
				threeKind = threeKind +1; 
			}
			else if ((val1 == val2 && val3 == val4) || (val1 == val2 && val4 == val5) || (val2==val3 && val4 == val5)) {
				twoPair = twoPair + 1; 
			}
			else if (val1 == val2 || val2 == val3 || val3 == val4 || val4 == val5) {
				onePair = onePair + 1; 
			}
			
		}
		
    // Calculates the probabilities of each respective hand
		double prob4 = (double)(fourKind) / (double)(numGen); 
		double prob3 = (double)(threeKind) / (double)(numGen);
		double prob2 = (double)(twoPair) / (double)(numGen);
		double prob1 = (double)(onePair) / (double)(numGen);
		
    //Output statements
		System.out.println("The number of loops: " + numGen);
		System.out.printf("The probability of Four-of-a-kind: %2.3f\n" , prob4);
		System.out.printf("The probability of Three-of-a-kind: %2.3f\n" , prob3);
		System.out.printf("The probability of Two-pair: %2.3f\n" , prob2);
		System.out.printf("The probability of One-pair: %2.3f\n" , prob1);
					
	}
				
}