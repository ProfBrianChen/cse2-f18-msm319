public class ArraysAndMethods {
	public static int[] copy(int[] array0) {
		int[] retArray = new int[array0.length]; //initializes a return array that will have the same number as elements as the original
		for (int i = 0; i < array0.length; i++) { 
			retArray[i] = array0[i]; // Assigns the values in all the elements of the original array into the second array to be returned
		}
		return retArray; // returns the return array
	}
	public static void inverter(int[] array0) {
		int last = array0.length - 1; //Creates an int value for the last elements number in the array (9 in this case)
		int tempVal; //variable will be used as a temporary variable for swapping variables
		for (int i=0; i < array0.length/2; i++) {
			tempVal = array0[i]; //temporary variable holds the value of the current element
			array0[i] = array0[last]; //the current element is assign with the opposite element value (0 gets 9 value, 1 gets 8 value ect)
			array0[last] = tempVal; //the opposite element value picks up the value from the current element
			last--; //decrements the last value to be swapped
		}
	}
	public static int[] inverter2(int[] array) {
		int[] retArray = copy(array); //copies the array that is sent through to this method
		int last = retArray.length - 1; // same as inverter
		int tempVal;
		for (int i=0; i < retArray.length/2; i++) {
			tempVal = retArray[i];
			retArray[i] = retArray[last];
			retArray[last] = tempVal;
			last--;
		}
		return retArray; // returns the copied array to the main method
	}
	public static void print(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " "); //for loop iterates through each value and prints it out for as many times as the length of the array
		}
		System.out.println(); //newline
	}

	public static void main(String[] args) {
		int[] array0 = {1,6,5,10,9,20,19,5,0,21}; //placing values in for the first array
		int[] array1 = copy(array0); // copying first array into array1
		int[] array2 = copy(array0); // copying first array into array 2
		inverter(array0); //calling inverter and sending value in the original array
		print(array0); //prints the array that is changed after passing through inverter
		print(inverter2(array1)); //prints the returned array from inverter 2
		int [] array3 = inverter2(array2); // assigns array3 with the array returned by inverter 2
		print(array3); //prints the array
	}
}