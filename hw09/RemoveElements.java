import java.util.Scanner;
import java.util.Random;

public class RemoveElements {
	public static String listArray(int num[]){ // this method is copied from the homework statement
		String out="{";
		for(int j=0;j<num.length;j++){
		  if(j>0){
		    out+=", ";
		  }
		  	out+=num[j];
		  }
			out+="} ";
			return out;
	}
	public static int[] randomInput() { // method used to input random numbers in the array
		int num[]=new int[10]; // creates new array since no array is sent from the method
		Random rand = new Random(); //random number generator
		for (int i = 0; i<num.length; i++) { // iterates the length of the array (10)
			num[i] = rand.nextInt(10); // assigns the current element a random value from 0-9
		}
		return num; // returns the array to main method
	}
	public static int[] delete(int[] list, int pos) {
		int copy[]=new int[9]; // new array creates with one fewer elements than the list array
		int k = 0; 
		if (pos<0 || pos>9) { // if the position requested to change isn't even an element in the array this if statemnt will just return the array without changes
			System.out.println("The index is not valid.");
			return list; // returns the unchanged array
		}
		else { // if the position is actually valid
			System.out.println("Index " +pos+" element is removed");
			for (int i = 0; i < list.length; i++) { // for loop goes through all the elements in the array
				if (i == pos) { // if the current elements is the element that the user wanted to delete
					i++; // increments i so that the element is skipped
					copy[k]= list[i]; // here, k is used to maintain the elements in the copied array while the other array skips an element, so that the copied array will only have 9 elements
				}
				else {
					copy[k]=list[i]; //copies all values from the old array to the copied array
				}
				k++; // increments k
			}
			return copy; // returns the copied array
		}
	}
	
	public static int[] remove(int[] list, int target) {
		int occur = 0; //says the amount of occurences so that the copied array could have the list array - occurences number of elements
		int count = 0; 
		int k = 0;
		for (int i = 0; i < list.length; i++) {
			if (list[i] == target) {
				occur++; // if the value is found the number of occurences increments
			}
		}
		int copy2[]=new int[10 - occur]; // creates new array with the correct number of elements
		for (int i = 0; i < list.length; i++) {
			if (list[i] == target) {
				count++; // only counts if the value is found
				i++; // increments i to skip the current element
        if (list[i] == target) { // incase there are two same values back to back
					count++;
					i++;
					copy2[k]= list[i];
				}
				copy2[k]= list[i]; // assigns element of the current variable 
			}
			else {
				copy2[k] = list[i]; // copies elements from the old array to the new one
			}
			k++; // increments k
		}
		if (count==0) { // since count was only incremented if a value was found, it would be 0 if no values were found
			System.out.println("Element " +target+ " was not found");
		}
		else {
			System.out.println("Element " +target+ " has been found");
		}
		return copy2; // returns the copied array
		
	}

	public static void main(String[] args) { // below code is untouched code copied from the homework description
		@SuppressWarnings("resource")
		Scanner scan=new Scanner(System.in);
		int num[]=new int[10];
		int newArray1[];
		int newArray2[];
		int index,target;
		String answer="";
		do{
		  	System.out.println("Random input 10 ints [0-9]");
		  	num = randomInput();
		  	String out = "The original array is:";
		  	out += listArray(num);
		  	System.out.println(out);
		 
		  	System.out.print("Enter the index ");
		  	index = scan.nextInt();
		  	newArray1 = delete(num,index);
		  	String out1="The output array is ";
		  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
		  	System.out.println(out1);
		 
		    System.out.print("Enter the target value ");
		  	target = scan.nextInt();
		  	newArray2 = remove(num,target);
		  	String out2="The output array is ";
		  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
		  	System.out.println(out2);
		  	 
		  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
		  	answer=scan.next();
			}while(answer.equals("Y") || answer.equals("y"));
		  }

}
