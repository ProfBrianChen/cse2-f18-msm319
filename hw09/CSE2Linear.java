import java.util.Random;
import java.util.Scanner;

public class CSE2Linear {
	public static int linear(int array[], int num) {
		  int iteration = 0; // variable iteration made to count the amount of iterations
	      for (int i = 0; i < 15; ++i) { // for loop goes through each element in the array
	    	 iteration ++; // increments iteration after each iteration of the for loop
	         if (array[i] == num) { 
	            return iteration; // returns the current iteration the for loop is in if the number is found
	         }
	      }
	      
	      return -15; /* not found */
	   }
	
	public static int binary(int array [], int num) {
	      int mid; // variable for the mid value
	      int low; // variable for the low value
	      int high; // variable for the high value
	      int iteration = 0; // variable for the iteration
	      
	      low = 0; // low value set to the first element in the array
	      high = 15 - 1; // high value set to the highest value in the array

	      while (high >= low) { // Maximum 4 iterations allowed
	    	 iteration++; // increments
	         mid = (high + low) / 2; // finds the midpoint
	         if (array[mid] < num) { // if the midpoint is less than the number the low value increments
	            low = mid + 1;
	         } 
	         else if (array[mid] > num) { // if the mid value is higher than the number then the high value decrements
	            high = mid - 1;
	         } 
	         else {
	            return iteration; // if midpoint matches the number, it returns the current iteration
	         }
	      }

	      return -4; // not found
	   }
	
	public static int[] scramble(int[] array) {
		int temp = 0; // variable for temporary value
		Random rand = new Random(); // random number generator
		System.out.println("Scrambled"); //print
		
    // for loop scrambles the current array by swapping a random element with the first element
		for (int i = 0; i < 30; i++) {
			int  randNum = rand.nextInt(15); // generates random value frm 0-14
			temp = array[0]; // temporary value used to swap values
			array[0]= array[randNum]; //first element swapped with the element that was generates by random number
			array[randNum] = temp; // random number element swapped with value from the first element
		}
		return array; // returns this array to the main method
	}
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in); // set up scanner
		int[] array = new int[15]; // create new array with 15 elements
		int input; // create variable
		boolean isInt; // boolean variable used to see if the int is actually an int
		int k; 
		System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
		for (int i = 0; i < 15; i++) {
			while (true) { // will be stuck on this while loop unless int meets all requirements
				k = i;
				isInt = scan.hasNextInt(); // will return true if int but false if not
				if (isInt != true) { // goes into this if statement if the entered value isn't an int
					System.out.println("Not an int, input another one: ");
					scan.next(); // clears the "conveyer belt"
					continue; // continue function puts us back to the beginning of the while loop, because if the value isn't an int it would cause errors in the other two requirements
				}
				input = scan.nextInt(); // if the value is actually an int it will be assigned to input variable 
				while ( 0 > input || input >100) { // tests to see if the int inputed is between 0 and 100
					System.out.println("Not between 0-100, input another int: ");
					isInt = scan.hasNextInt(); // makes sure the inputed value is an int
					while (isInt != true) { // only break out of the loop if the value is an int
						System.out.println("Not an int, input another one: "); 
            scan.next(); // clears the conveyer belt
            isInt = scan.hasNextInt(); // gets new boolean condition if the entered value is an int or not
					}
					input = scan.nextInt(); // if the value is an int it will be passed to the variable input
				}
				if (i != 0) { // i is only equal 0 in the first element, other elements will fall in this if statement
					k--; // recall k is equal to i, so decrementing k is one value less than i
					if (array[k] <= input) { // if statement ensures the inputed value is higher than the previous element
						break; // breaks out of while loop
					}
					else {
						System.out.println("Entered int isn't greater or equal to previous int, enter a new one: "); // after this statement the flow returns back to start of the while loop
					}	
				}
				else { 
					break; // this else loop is only for the first element in the array
				}
			}
			array[i] = input; // once the above while loop is broken, the current element of the array is assigned with the user inputed value once it passes all the requirements
		}
		
	System.out.println("Enter a grade to search for: ");
	int grade = scan.nextInt(); // gets grade to search for by the user
	int iteration = binary(array, grade); //calls binary method, sending in the array and the grade entered
	if (iteration == -4) { // if the returned value is -4 it means the value wasn't found
		iteration = iteration * -1; // make the number of iterations positive
		System.out.println(grade + " was not found in the list with "+ iteration+ " iterations");
	}
	else {
		System.out.println(grade + " was found in the list with "+ iteration+ " iterations");
	}
	scramble(array); // scrambles the array with the scramble method
	for (int i=0; i< array.length; i++){ 
		  System.out.print(array[i]+" ");  // prints the array
		} 
	System.out.println(); // newline
	System.out.println("Enter a grade to search for: ");
	int grade2 = scan.nextInt(); // gets grade 2 from the user
	int iteration2 = linear(array, grade2); // calls linear method and gives the returns value to the variable iteration 2
	if (iteration2 == -15) { // not found
		iteration2 = iteration2 * -1; // makes it posittive
		System.out.println(grade2 + " was not found in the list with "+ iteration2+ " iterations");
	}
	else {
		System.out.println(grade2 + " was found in the list with "+ iteration2+ " iterations");
	}
	
	
	}
}
