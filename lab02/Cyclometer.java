// Cyclometer program will say the amount of time for a bike trip, number of wheel rotations, distance traveled in miles
// and distance for two trips combined
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
       
    int secsTrip1=480;  // this interger states the number of seconds in the first trip
    int secsTrip2=3220;  // this integer states the number of seconds in the second trip
		int countsTrip1=1561;  // this integer states the number of counts (wheel rotations) in the first trip
		int countsTrip2=9037; // this integer states the number of counts (wheel rotations) in the second trip
      
    double wheelDiameter=27.0;  // This value states the wheel diameter (in inches)
  	float PI=3.14159f; // states the value of pi
  	double feetPerMile=5280;  // Value states the amount of feet in a mile for a conversion later
    double inchesPerFoot=12;  // States the amount of inches in a foot for a conversion
  	double secondsPerMinute=60;  // States the amount of seconds in a minute
  	double distanceTrip1, distanceTrip2,totalDistance;  // Creates variables capable of decimal places for the distance of trip 1, trip 2, and total distanceTrip1
    
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); // Print statement tells how long trip 1 took in minutes as well as the count 
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); // Print statement tells how long trip 2 took in minutes as well as the count 
      
		  // Calculating the distance of trip one by multiplying the diameter of the wheel by pi (to get the circumference)
      // and then finally multiplying the circumference by the number of wheel rotations to get the distance (in inches)
	    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Distance of trip 2 in miles
	    totalDistance=distanceTrip1+distanceTrip2; //Sums up the distances of trip one and two to create the total distance
      
      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles"); //Prints the distance of trip 1 in miles
    	System.out.println("Trip 2 was "+distanceTrip2+" miles"); //Prints out the distance of trip 2 in miles
	    System.out.println("The total distance was "+totalDistance+" miles"); //Prints out the total distance in miles



      
      
	}  //end of main method   
} //end of class
