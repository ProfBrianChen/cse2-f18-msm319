//Second part of homework assignment #3, this code will calculate the volume of a pyramid
// The code will prompt the user to input values for the dimension of the pyramid too
import java.util.Scanner; //Imports the scanner function 
public class Pyramid {
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //Declares and instance of the scanner function
          System.out.print("Input the length of the square side of the pyramid: ");// promts the user to input a value for the length of the square side of the pyramid
          double squareLength = myScanner.nextDouble();//Allows user to input value
          System.out.print("Input the height of the pyramid: "); //Prompts user to input a value for the height of the pyramid
          double pyramidHeight = myScanner.nextDouble(); //Allows user to input the value
          double pyramidArea = Math.pow(squareLength, 2); //Calculates the area of the pyramid base
          double pyramidVolume = pyramidArea * pyramidHeight / 3; //Calculates the volume of the pyramidVolume
          System.out.println("The volume inside of the pyramid is: " + pyramidVolume); //Outputs the pyramid volume
          
}  //end of main method   
  	} //end of class