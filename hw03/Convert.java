//First part of homework assignment #3, this code will convert the amount of rain fall in an area into cubic miles
//Program will ask the user to imput values for both the affected area as well as how many inches of rain fell
import java.util.Scanner; //Imports the scanner function 
public class Convert {
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //Declares and instance of the scanner function
          System.out.print("Enter the affected area in acres: ");//prompt the user to input value for the acerage of the affected area
          double rainArea = myScanner.nextDouble();//creates double variable for rainArea as well as lets user input value
          System.out.print("Enter the rainfall in the affected area: "); //promt the user to input a value for the inches of rain in the area
          double rainFall = myScanner.nextDouble();//creates double variable for the inches of rainfall
          double cubicMiles; //create variable cubicMiles
          double rainAreaMiles = rainArea * 0.0015625; //converts the area into miles
          double rainFallMiles = rainFall * 1.57828E-5; //converts the rainfall into miles
          cubicMiles = rainAreaMiles * rainFallMiles; //calculates the total amount of rain in cubic miles
          System.out.println("The total amount of rain is: " + cubicMiles + " cubic miles"); //Prints out the value for cubicMiles

}  //end of main method   
  	} //end of class