import java.util.Scanner; // imports the scanner function for this file
// This program helps people split a check evenly after a tip has been made
// This program asks the user to input the cost, percent tip, and the number of people who went to dinner
// and prints out how much each person owes 
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //Declares and instance of the scanner function
          System.out.print("Enter the original cost of the check in the form xx.xx: "); //promts the user to input a value for the check cost
          double checkCost = myScanner.nextDouble(); // User inputs value for the double checkCost
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):" ); //Prompts the user to input a value for the tip percent
          double tipPercent = myScanner.nextDouble(); //User inputs value for the tip percent
          tipPercent /= 100; //We want to convert the percentage into a decimal value
          System.out.print("Enter the number of people who went out to dinner:" ); // Prompts the user to input a value for the number of people at dinner
          int numPeople = myScanner.nextInt();// User inputs value for the number of people at dinner
          double totalCost; //Creates double variable total cost
          double costPerPerson; //Creates double variable cost per person
          int dollars, dimes, pennies; // Creates integers for the values of dollars, dimes, and pennies
          totalCost = checkCost * (1 + tipPercent); // calculates the total cost of dinner with the tip
          costPerPerson = totalCost / numPeople; // calculates the total cost per person (we will later make this an integer so we can get two decimals)
          dollars=(int)costPerPerson; //get the whole amount, dropping decimal fraction
          dimes=(int)(costPerPerson * 10) % 10; //get dimes amount by taking the remainder of the cost per person in the dime slot
          pennies=(int)(costPerPerson * 100) % 10; //Calculates pennies amount by taking the remainder 
          System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //Prints the amount each person in the group owes
          

}  //end of main method   
  	} //end of class
