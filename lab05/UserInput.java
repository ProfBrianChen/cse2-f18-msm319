// The following lab asks the user a bunch of questions regarding course work, and only accepts answers if they 
// are in the right format (ie. if an int is asked an int must be provided)
import java.util.Scanner; //Imports scanner
public class UserInput {
  public static void main(String[] args){
    Scanner scnr = new Scanner(System.in); // Sets up scanner function
    
    System.out.println("Input your course number"); //prompts user to input course number
    int courseNumber = 1; // Sets arbitrary value for coursenumber to avoid not initialized variables
    boolean myCourse = scnr.hasNextInt(); // Sets boolean value for if the variable entered was indeed an integer
    boolean keepGoing = true; // arbitrary true value
    //All of the rest of the while statments for the other formats are based the same as this one. 
    // Basically, if the user did input the correct variable type, then the while loop breaks, if the user didn't,
    // then the while loop keeps asking until the correct variable type is entered. In this case, if an integer
    // isn't submitted, then myCourse will never be true and the while loop will never break. The keep going boolean
    // just allows us to enter the while loop, but its up to the user actually following what was asked to break
    // out of the while loop.
    while(keepGoing){
      if (myCourse){
        courseNumber = scnr.nextInt();
        break; 
      }
      else{
        System.out.println("Invalid Course number, Input another one");
        scnr.next();
        myCourse = scnr.hasNextInt(); 
      } 
    }
    scnr.nextLine(); // Before all the string statements this next line function must be used. If not used then the variable
    // won't pick up the whole line of the string value entered. The rest of the code follows the same format, 
    // the only differences is that a haNextLine is used for strings while a hasNextInt is used for integers. 
    System.out.println("Input your department name"); 
    String myDepartment = "initialized";
    boolean department = scnr.hasNextLine(); 
    while(keepGoing){
      if (department){
        myDepartment = scnr.nextLine(); // sets variable equal to the whole line, including multiple words and spaces
        break; 
      }
      else{ // Important note is that it probably will never enter this loop since numbers can be strings too
        System.out.println("Invalid Department, Input another one");
        scnr.nextLine();
        department = scnr.hasNextLine(); 
      } 
    }
    
    System.out.println("How many times does the class meet a week?"); 
    int meetingNum = 1;
    boolean meeting = scnr.hasNextInt(); 
    while(keepGoing){
      if (meeting){
        meetingNum = scnr.nextInt();
        break; 
      }
      else{
        System.out.println("Not a real answer, enter how many times it meets a week");
        scnr.next();
        meeting = scnr.hasNextInt(); 
      } 
    }
    
    System.out.println("What time does the class start?"); 
    int meetingTime = 1;
    boolean time = scnr.hasNextInt(); 
    while(keepGoing){
      if (time){
        meetingTime = scnr.nextInt();
        break; 
      }
      else{
        System.out.println("Not a real answer, enter what time the class starts");
        scnr.next();
        time = scnr.hasNextInt(); 
      } 
    }
    
    scnr.nextLine();
    System.out.println("What is your professor's name?"); 
    String profName = "name";
    boolean name = scnr.hasNext(); 
    while(keepGoing){
      if (name){
        profName = scnr.nextLine();
        break;  
      }
      else{
        System.out.println("Not a valid answer, enter your professor's name");
        scnr.nextLine();
        name = scnr.hasNextLine(); 
      } 
    }
 
    
    System.out.println("How many students are in the class?"); 
    int numStudents = 1;
    boolean students = scnr.hasNextInt(); 
    while(keepGoing){
      if (students){
        numStudents = scnr.nextInt();
        break; 
      }
      else{
        System.out.println("Not a real answer, enter the number of students");
        scnr.next();
        students = scnr.hasNextInt(); 
      } 
    }
    
    // Set up an output statement that states all the values that were given in the code. 
     System.out.println("The course number is " + courseNumber + " in the " + myDepartment + " department, which meets " + meetingNum + " per week at " + meetingTime + ". The class is taught by " + profName + " and has " +numStudents + " students in it.");
    
    
    
  }
}