import java.util.Scanner;
import java.util.Random; 

public class Shuffling {
	
	public static void printArray(String[] cards) { 
		for (int i=0; i< cards.length; i++){ //For loop iterates as many times as there are terms in the array
			  System.out.print(cards[i]+" "); // Prints out values in the array with a space in between
			} 
		System.out.println(); //newline
		
	}
	public static String[] shuffle(String[] cards) {
		String temp = "Initialize"; //Initialize a temporary string variable called temp
		Random rand = new Random(); // setting up a random number generator
		System.out.println("Shuffled"); // print statement
		
		for (int i = 0; i < 70; i++) { // for loop to shuffle 70 times
			int  randNum = rand.nextInt(51) + 1; // generates a random number from 0-51 for each iteration of the for loop
			temp = cards[0]; // swapping variables to the randomly generated card to the first value in the array
			cards[0]= cards[randNum]; // swapping
			cards[randNum] = temp; //using temporary value to store the string
		}
		return cards; // returns the new array to the main method
	}
	public static String[] getHand(String[] cards, int index, int numCards) {
		System.out.println("Hand"); //print
		String[] hand = new String[5]; //creating an array hand since it wasn't sent over from the main method
		for (int i =0; i < numCards; i++) { //for loop iterates as many times as the hands
			hand[i] = cards[index]; //index starts at 51, and from the main method it decreases by 5 each time this method is called
			index--; //decrement 
		}
		return hand; //returns the hand array
	}
	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		 //suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
		  cards[i]=rankNames[i%13]+suitNames[i/13]; 
		  //System.out.print(cards[i]+" "); I took this print statement out because there was redundancy, it didn't match the example output from the example
		} 
		//System.out.println(); Also took this newline out
		printArray(cards); 
		shuffle(cards); 
		printArray(cards); 
		while(again == 1){ 
		   hand = getHand(cards,index,numCards); 
		   printArray(hand);
		   index = index - numCards;
		   if (index - numCards < 0) { // Because the getHand method only returns the hand string, this if statement had to be placed
         // in the main method so that the index will be reset to 51 if it gets negative with the next iteration. 
				index = 51; 
				shuffle(cards); // shuffles a new deck
			}
		   System.out.println("Enter a 1 if you want another hand drawn"); 
		   again = scan.nextInt(); 
		}   
		}


	}